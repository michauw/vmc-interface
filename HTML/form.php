<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="de_DE" xml:lang="de_DE">
<!--
 * Created on 10.01.2007
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
				-->
<head>
	<meta http-equiv="content-type" content="text/html; charset=<?php echo $ENCODING; ?>" />
	<title><?php echo $PAGE_TITLE; ?></title>
	<link rel="shortcut icon" href="images/favicon.ico" />
	<link rel="stylesheet" href="<?php echo $CSS_LINK; ?>" type="text/css"></link>
	<link rel="stylesheet" href="CSS/form.css" type="text/css"></link>
	<link rel="stylesheet" href="CSS/keyboard.css" type="text/css"></link>

	<link rel="stylesheet" type="text/css" href="CSS/jquery-ui-1.8.17.custom.css">
	<link href='http://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/jquery-ui.min.js"></script>

	<!--<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>-->
	<script type="text/javascript" src="js/jsfunctions.js"></script>
	<script type="text/javascript" src="js/initDialogs.js"></script>
	<script type="text/javascript" src="js/keyboard.js"></script>

</head>
<body style=" color:#663300; ">
  	 <!--<h3><?php echo $PAGE_HEADLINE; ?></h3>-->
	 <!--<p> <?php echo $PAGE_CONTENT; ?> </p>-->


	<!--<?php echo $FORM_DATA; ?>-->
	<div id="searchFields" style="float:left; width:658px;">
		<a href = "#" id="logo"> <img style=" z-index:10; position:fixed; opacity:0.4; top:0px; left:5px;"; src="images/logo_base74.png"></img></a>
		<div class = "gradient-title shadow" style=" width:99.8%; top:0px; left:1px; position:fixed; height:74px; padding-top:5px; border-style:solid; border-width:1px; border-color:#663300;">
			<a href = "http://www.dfg.de/index.jsp"> <img style=" z-index:10; opacity:0.4; margin-top:10px; margin-right:15px; height:50px; float:right"; src="images/dfg_logo.png"></img> </a>
			<a href = "http://www.uni-freiburg.de/"> <img style=" z-index:10; opacity:0.4; margin-top:10px; margin-right:20px; float:right"; src="images/logo.png"></img> </a>


			<div style="top:10px; font-family: 'Times New Roman', serif;font-size:20px;margin-left:40%;" > 
				<em id="title"; style="font-family:slavicfont; font-size:25px"></em>
				<br>
				<span style="margin-left:30px">VM&#x10c; Query interface <span>
			</div>
		</div>
		<span>
		<form class="corpusform"; id="corpusformExact"; name="tokwic"; action="results.php"; method="POST"; target="_blank"> 

				
				<br/>
				<!--  ExactPhrase Box-->
				<div  id="bigBox1" class = "shadow gradient" style="">
					<a href = "#" id="info1" style="margin-bottom:2px; margin-right:4px; float:left;"><img name="extendedImage" src="images/info.png" title="Advanced search"></a>
				<div style="font-family:'EB Garamond', serif; font-size:24px;margin-left:38%;" >Simple search </div>
					<div id="box1" style="width:99%; float:left;">
						<!--<label id ="l1" style="font-weight: solid; font-family: arial; font-size:11px; margin-left:10px; width:94px" ></label>-->
						<input style="width:584px; margin-left:5px; float:left";type="text" id="exactQ" onkeyup=getSuggestion("exactQ") name="queryExact" class="keyboardInput" title="Enter your query as exact phrase" />
						<br/>
						<br/>
						<input type="checkbox" style="margin-left:10px; width:10px;" name="checkBox0"  value="yes2" />
						<label style="font-weight: lighter; font-family:'EB Garamond', serif; font-size:12px; margin-left:10px; width:75px;"> case sensitive</label>

					</div>
					<br />
					<input class = "gradient-btn"; style="font-family:'EB Garamond', serif; font-weight:bold; float:right"; type="submit" name="btn[xml]" onclick=exactPhraseQuery() width="100" value=" Search "/>
					<input class = "gradient-btn"; style="font-family:'EB Garamond', serif; font-weight:bold; float:right"; type="submit" name="btn[xmlfile]" onclick=exactPhraseQuery() width="100" value=" Export XML "/>
				</div>
				<input type="text" id="outQueryExact" name="query" style="visibility:collapse"/>
		</form>
		 </span>
		<span>
		<form class="corpusform" id="corpusformAdvanced" name="tokwic" action="results.php" method="POST" target="_blank"></br>

				<!--  Advancedsearch Box-->
				<div class = "shadow gradient" id="bigBox2">
					<a href = "#" id="info2" style="margin-bottom:2px; margin-right:4px; float:left;"><img name="extendedImage" src="images/info.png" title="Advanced search"></a>
					<div style="font-family:'EB Garamond', serif; font-size:24px; margin-left:36%" > Advanced search </div>
					<div id="box2" style="width:100%; float: left;">
						<label style="float-left; font-weight: bold; font-family:'EB Garamond', serif; font-size:15px; margin-left:10px; width:auto;"> 1. Word </label>
						<input  style="width:522px; margin-left:6px; float:left";type="text" type="text" id="bQBegin" onkeyup=getSuggestion("bQBegin") name="query_begin" class="keyboardInput" />
						<br />
						<br/>
						<input type="checkbox" style="margin-left:10px; width:10px;" id="checkBox1" name="checkBox1" value="yes1"/>
						<label style="font-weight: lighter; font-family:'EB Garamond', serif; font-size:12px; margin-left:10px; width:70px;"> begins with</label>
						<input type="checkbox" style=" width:10px;" name="checkBox1" id="checkBox1" value="yes2" />
						<label style="font-weight: lighter; font-family:'EB Garamond', serif; font-size:12px; margin-left:10px; width:65px;"> ends with</label>
						<input type="checkbox" style=" width:10px;" name="checkBox1" id="checkBox1" value="yes3" />
						<label style="font-weight: lighter; font-family:'EB Garamond', serif; font-size:12px; margin-left:10px; width:75px;"> case sensitive</label>
						<br/>
						<div  class="gradient-opacity"; style="float:left; width:570px; height:34px;">
							<label style="margin-left: 250px; width:140px; font-weight: bold; font-family:'EB Garamond', serif; font-size:15px;">Tokens in between:</label>
							<label style="font-weight: lighter; font-family:'EB Garamond', serif; font-size:12px; width:35px; margin-left:10px;" > from </label>
							<input type="text" id="bQFrom2" style="font-size:12px; font-family:Garamond, serif; width:30px;" name="bound_begin" value="0"/>
							<label style="font-weight: lighter; font-family:'EB Garamond', serif; font-size:12px; width:30px; text-align:center">to</label>
							<input type="text" id="bQTo2" style="font-size:12px; font-family:Garamond, serif; width:30px;" name="bound_end" value="0"/>
						</div>
						<div  class="gradient-opacity2"; style="float:left; width:30px; height:34px;"></div>
						<br />
						<label style="float-left;  font-weight: bold; font-family:'EB Garamond', serif; font-size:15px; margin-left:10px; width:auto;"> 2. Word</label>
						<input style="width:522px; margin-left:5px; float:left"; type="text" id="bQEnd2"  onkeyup=getSuggestion("bQEnd2")  name="query_end" class="keyboardInput" />
						<br />
						<br/>
						<input type="checkbox" style="margin-left:10px; width:10px;" name="checkBox2" value="yes1"/>
						<label style="font-weight: lighter; font-family:'EB Garamond', serif; font-size:12px; margin-left:10px; width:70px;"> begins with</label>
						<input type="checkbox" style=" width:10px;" name="checkBox2"  value="yes2" />
						<label style="font-weight: lighter; font-family:'EB Garamond', serif; font-size:12px; margin-left:10px; width:65px;"> ends with</label>
					</div>
					<br />
					<a href = "#" id="down" style="margin-bottom:5px; margin-top:8px; margin-right:4px; margin-left:0px; float:left;" onclick = openBox()><img name="extendedImage" src="images/down.png" title="Advanced search"></a>
					<a href = "#" id="up" style="margin-bottom:5px; margin-top:8px; margin-right:4px; float:left;" onclick = closeBox()><img name="extendedImage" src="images/up.png" title="Advanced search"></a>
					<input class = "gradient-btn"; style="font-family:'EB Garamond', serif; font-weight:bold; float:right"; type="submit" id="boundBtn" name="btn[xml]" onclick=boundQuery() width="100" value=" Search "/>
					<input class = "gradient-btn"; style="font-family:'EB Garamond', serif; font-weight:bold; float:right"; type="submit" id="boundBtnXml" name="btn[xmlfile]" onclick=boundQuery() width="100" value=" Export XML "/>
				</div>
				<input type="text" id="outQueryAdvanced" name="query" style="visibility:collapse"/>
		</form>
		</span>
		<span>
		<form class="corpusform" id="corpusformCQP" name="tokwic" action="results.php" method="POST" target="_blank"></br>

				<!--  QCP Box-->
				<div class = "shadow gradient" id="bigBoxQCP">
					<a href = "#" id="info3" style="margin-bottom:2px; margin-right:4px; float:left;"><img name="extendedImage" src="images/info.png" title="Advanced search"></a>
					<div style="font-family:'EB Garamond', serif; font-size:24px; margin-left:37%" > Complex search </div>
					<div id="boxQCP" style="width:99%; float: left; rgb(251,157,35); ">
						<!--<label style="font-weight: solid; font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif; font-size:11px; margin-left:10px; width:94px"> CQP Query:</label>-->
						<input style="width:584px; margin-left:5px; float:left"; type="text" id="qcp" name="query_qcp" class="keyboardInput" />
					</div>
					<br />
					<input class = "gradient-btn"; style="font-family:'EB Garamond', serif; font-weight:bold; float:right"; type="submit" id="qcpBtn" name="btn[xml]" onclick=qcpQuery() width="100" value=" Search " />
					<input class = "gradient-btn"; style="font-family:'EB Garamond', serif; font-weight:bold; float:right"; type="submit" id="qcpBtnXml" style="" name="btn[xmlfile]" onclick=qcpQuery() width="100" value=" Export XML "/>
				</div>
				<input type="text" id="outQueryCQP" name="query" style="visibility:collapse"/>
				<br />
		</form>
		</div>
		</span>
		<!-- Information Box-->
		<div id="infoBigBox"; class = "shadow gradient" style="margin-top:18px; float:right; height:83%; padding-left:5px; padding-top:5px; border-style:solid; border-width:1px; border-color:#663300;">
			<div style="font-family:'EB Garamond', serif; font-size:24px; margin-left:30%;" >Welcome to the VM&#x10c; Query interface!</div>
			<div id="infoBox" style="overflow-y:scroll; position:inherit; width:98.5%; hight:100px; padding-left:5px; float: left;">
			</div>
		</div>
		<!-- <input type="text" id="outQuery" name="query" style="visibility:collapse"/>-->
		<br />
		<!-Abmelden-panel-->
		<div class = "gradient-title shadow login" style="font-family:'EB Garamond', serif; font-weight:bold; opacity:1; bottom:0px; left:1px; position:fixed; height:30px; padding-left:5px; border-style:solid; border-width:1px; border-color:#663300;">
		      <input class = "gradient-btn"; type="submit" value="log out" ONCLICK="window.location.href='logout.php'" />
		</div>
</body>
</html>

