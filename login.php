<?php
     if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      session_start();

      $username = $_POST['username'];
      $passwort = $_POST['passwort'];

      $hostname = $_SERVER['HTTP_HOST'];
      $path = dirname($_SERVER['PHP_SELF']);

      // Benutzername und Passwort werden �berpr�ft
      $users = file ('users.txt');
      foreach ($users as &$value) {
	$user = explode(" ", $value);
       $user[1]=preg_replace("#[[:cntrl:]]#", "", $user[1]);
	if (strcmp($username,$user[0])==0 && strcmp($passwort,$user[1])==0) {
        $_SESSION['angemeldet'] = true;

        // Weiterleitung zur gesch�tzten Startseite
        if ($_SERVER['SERVER_PROTOCOL'] == 'HTTP/1.1') {
         if (php_sapi_name() == 'cgi') {
          header('Status: 303 See Other');
          }
         else {
          header('HTTP/1.1 303 See Other');
          }
         }

        header('Location: http://'.$hostname.($path == '/' ? '' : $path).'/index.php');
        exit;
       }
      }
     }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
 <head>
	<link rel="shortcut icon" href="images/favicon.ico" />
	<link rel="stylesheet" href="<?php echo $CSS_LINK; ?>" type="text/css"></link>
	<link rel="stylesheet" href="CSS/form.css" type="text/css"></link>
	<link rel="stylesheet" href="CSS/login.css" type="text/css"></link>
	<link rel="stylesheet" href="CSS/keyboard.css" type="text/css"></link>

	<link rel="stylesheet" type="text/css" href="CSS/jquery-ui-1.8.17.custom.css">
	<link href='http://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/jquery-ui.min.js"></script>
	<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-36597276-1']);
		  _gaq.push(['_trackPageview']);
		  (function() {
			    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
	</script>

	<!--<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>-->
	<script type="text/javascript" src="js/welcomeDialog.js"></script>

	<title>VMC Query Interface</title>
	<meta name="description" content="\u0412\u0435\u043B\u0438\u043A\u0438\u0435\ \u041C\u0438\u043D\u0435\u0438\ \u0427\u0435\u0442\u044C\u0438\ Welcome to the VM\u010C\Corpus! The  VM\u010C\ represent one of the most important monuments of Russian Church Slavonic literature compiled by Macarius, Metropolitan of Moscow and his scribes in the 16th century.  
			The VM\u010C\ corpus is being developed as part of The VM\u010C\ are organized according to months and days of the calendar year. On this website you can access the Acts of the Apostles with patristic commentaries, Pauline epistles, 
			and the Ecumenical letters, all of which relate to the 30th of June (Weiher et al. 1997; Rabus et. al 2012).  
	  	      VM\u010C\ development team:  
          	    	- Dr. Ruprecht von Waldenfels (overall concept and project supervisor) 
       	       - Prof. Dr. Achim Rabus (consultation and specifications) 
	              - Simon Skilevic, B.A. (programming and design)
	              - Stefan Savi\u0107\, M.A. (texts and design)  
       	        Bibliography:  
	              Rabus, A., Savi\u0107\, S., Waldenfels, R. v. 2012. Towards an electronic corpus of the Velikie Minei \u010C\et\u0027\i. In: Rediscovery: Bulgarian Codex Suprasliensis of the 10th century. Sofia: Iztok Zapad.
	              Waldenfels, R. v., Rabus, A. 2015. Recycling the Metropolitan: Building an Electronic Corpus on the Basis of the Edition of the Velikie Minei \u010C\et\u0027\i. In: Scripta \u0026\ e-Scripta 14-15\u2044\2015, 27-38.
              	  Weiher, E. et al. 1997, 1998, 2001, 2007, 2009. Die Gro\u00DF\en Lesemen\u00E4\en des Metropoliten Makarij. 1-31 M\u00e4\u0072\z, 1-23 Mai. Freiburg i. Breisgau.: Weiher. ">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8">
 </head>
 <body style=" color:#663300; ">
   		<!-- upper panel-->
   		<a href = "#" id="logo"> <img style=" z-index:10; position:fixed; opacity:0.4; top:0px; left:5px;"; src="images/logo_base74.png"></img></a>
		<div class = "gradient-title shadow" style=" width:99.8%; top:0px; left:1px; position:fixed; height:74px; padding-top:5px; border-style:solid; border-width:1px; border-color:#663300;">
			<a href = "http://www.dfg.de/index.jsp"> <img style=" z-index:10; opacity:0.4; margin-top:10px; margin-right:15px; height:50px; float:right"; src="images/dfg_logo.png"></img> </a>
			<a href = "http://www.uni-freiburg.de/"> <img style=" z-index:10; opacity:0.4; margin-top:10px; margin-right:20px; float:right"; src="images/logo.png"></img> </a>


			<div style="top:10px; font-family: 'Times New Roman', Times, serif;font-size:20px;margin-left:40%;" > 
				<em id="title"; style="font-family:slavicfont; font-size:25px"></em>
				<br>
				<span style="margin-left:30px">VM&#x10c; Query interface <span>
			</div>
		</div>
		<!-- Information Box-->
		<div id="infoBigBox"; class = "shadow gradient" style="margin-top:18px; float:left; height:83%; padding-left:5px; padding-top:5px; border-style:solid; border-width:1px; border-color:#663300;">
			<div style="font-family:'EB Garamond', serif; font-size:24px; margin-left:36%;" >Welcome to the VM&#x10c; Query interface!</div>
			<div id="infoBox" style="overflow-y:scroll; position:inherit; width:98.5%; hight:100px; padding-left:5px; float: left;">
			</div>
		</div>
		<!--Anmeldungspanel-->
		<form class="login"; action="login.php" method="post">
			<div class = "gradient-title shadow" style="font-family:'EB Garamond', serif; font-weight:bold; opacity:1; bottom:0px; left:1px; position:fixed; height:30px; padding-left:20px; border-style:solid; border-width:1px; border-color:#663300;">
			     	Username <input style="margin-right:20px;" type="text" name="username" value="test"/>
	   			Password <input  style="margin-right:30px;" type="password" name="passwort" value="test"/>
		       	<input class = "gradient-btn"; style=""; type="submit" value="log in" />
			</div>
              </form>
  <!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
var sc_project=10496384; 
var sc_invisible=0; 
var sc_security="7c2bf9b1"; 
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" + scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a title="shopify traffic stats"
href="http://statcounter.com/shopify/" target="_blank"><img
class="statcounter" src="http://c.statcounter.com/10496384/0/7c2bf9b1/0/"
alt="shopify traffic stats"></a></div></noscript>
<!-- End of StatCounter Code for Default Guide -->
 </body>
</html>
