<?php
	 if($_SERVER['SERVER_NAME'] == "localhost") {
define('LOCALHOST', true);

} else {
define('LOCALHOST', false);
}

	if (LOCALHOST) {
		/* mysql configuration local */
		define('MYSQL_HOST', 'localhost'); //Server lokal
		define('MYSQL_USER', DB_USER_LOCAL); //Benutzername lokal
		define('MYSQL_PASS', DB_PWD_LOCAL); //Passwort lokal
		define('MYSQL_DATABASE', DB_NAME_LOCAL); //Name der Datenbank lokal
	} else {

		/* mysql configuration online */
		define('MYSQL_HOST', 'localhost'); //Server online
		define('MYSQL_USER', DB_USER); //Benutzername online //nksaadmin
		define('MYSQL_PASS', DB_PWD); //Passwort online
		define('MYSQL_DATABASE', DB_NAME); //Name der Datenbank online // nksasport
	}

     define('MYSQL_DSN', "mysql://".MYSQL_USER.":".MYSQL_PASS."@".MYSQL_HOST."/".MYSQL_DATABASE."");

	define('BASE_SCRIPT', $_SERVER['PHP_SELF']);
	define('HTTP_HOST', 'http://'.$_SERVER['SERVER_NAME']);  //Actual domain, without trailing slash eg. http://localhost, http://www.domain.com, http://127.0.0.1
	define('HTTP_BASE', substr(BASE_SCRIPT, 0, strrpos(BASE_SCRIPT, '/')).'/'); //Base directory of the System, with / on the end. If base directory is root put a slash ( / )
	define('WEB_FOLDER', str_replace(HTTP_HOST, "", HTTP_BASE));
	
	/* creating class objects */

$db = DB::connect(MYSQL_DSN);
if(DB::isError($db)) {
        die($db->getMessage());

}
?>