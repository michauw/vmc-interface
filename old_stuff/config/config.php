<?php
$path = '/usr/share/pear';
set_include_path(get_include_path() . PATH_SEPARATOR . $path);

require_once ('DB.php'); //class for database connection

require_once("const.inc.php");
require_once("config_mysql.php");


require_once("classes/Config.php");
require_once("functions.php");

define('ROOT', substr($_SERVER['SCRIPT_FILENAME'], 0, strrpos($_SERVER['SCRIPT_FILENAME'], '/')).'/'); //absoluter Pfad zum Wurzelverzeichnis   !case-sensitive auf LINUX
define('ACT_URL', HTTP_HOST.$_SERVER['REQUEST_URI']);
?>