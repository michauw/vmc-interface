<?php
        function marked_ids ($link_id) {
                global $db;
                global $tab_content;
                $marked = '';

                $marked .= $link_id;

                $sql = 'SELECT `parent_id` FROM `'.$tab_content.'` WHERE `link_id` = "'.$link_id.'"';
                $res = $db->query($sql);

                while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
                        if ($row['parent_id'] != 0) {
                                $marked .= ','.marked_ids($row['parent_id']);
                                }
                        }
                return $marked;
                }


        function getNavigationTitle ($link_id, $parent_id) {
		global $db;
		
		if ($parent_id == 0) {
				$id = $link_id;
		} else {
				$id = $parent_id;
		}
		
		$sql = 'SELECT `name` FROM `linksys_de` WHERE `link_id` = "'.$id.'"';
		$res = $db->query($sql);
		
		if ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
				return $row['name'];
		}
		
		return '';
 
}

        function infobox ($link, $content) {
                $infobox = '';

                $infobox .= '<table class="infobox-holder"><tr><td><a class="infobox" href="?'.$action.'"><img src="'.HTTP_HOST.HTTP_BASE.'homepage/img/icons/info.gif" border="0" /><span class="box">'.$content.'</span></a></td></tr></table>';

                return $infobox;
        }

        function insert_replaces($text) {
                $result = '';
                $sonderzeichen = array("\'", "'");
                $ersetzung   = array("'","\'");
                $result = str_replace($sonderzeichen, $ersetzung, $text);
                return $result;
        }

        function make_paths_absolute ($input) {
                // img only: preg_replace('/<img (.*?)src=\"(.*?)\"(.*?)>/', '<img $1src="'.HTTP_HOST.HTTP_BASE.'$2"$3>', $input);
                $output = preg_replace('/<(.*?)=\"http:\/\/userfiles\/(.*?)\"(.*?)>/', '<$1="'.HTTP_HOST.HTTP_BASE.'userfiles/$2"$3>', $input);
                $output = preg_replace('/<(.*?)=\"greuterhof\/userfiles\/(.*?)\"(.*?)>/', '<$1="'.HTTP_HOST.HTTP_BASE.'userfiles/$2"$3>', $output);
                $output = preg_replace('/<(.*?)=\"userfiles\/(.*?)\"(.*?)>/', '<$1="'.HTTP_HOST.HTTP_BASE.'userfiles/$2"$3>', $output);
                $output = preg_replace('/<(.*?) href=\"\/(.*?)\"(.*?)>/', '<$1 href="'.HTTP_HOST.HTTP_BASE.'$2"$3>', $output);
                $output = preg_replace('/<(.*?)=\"www/', '<$1="http://www', $output);
                return $output;
        }

        function img_size ($image) {
                $size_txt = '';

                $image = str_replace (HTTP_HOST.HTTP_BASE, '',$image );

                if (file_exists($image)) {
                        $size = getimagesize ($image);
                        $size_txt .= 'width="'.$size[0].'px" height="'.$size[1].'px"';
                }
                return $size_txt;
        }

        function make_img_max_width ($image, $max_width) {
                $size_txt = '';
                if (file_exists($image)) {
                        $size = getimagesize ($image);

                        if ($size[0] > $max_width) {
                        $height= ($max_width/$size[0])*$size[1];
                        $size_txt .= 'width="'.$max_width.'px" height="'.$height.'px"';
                        } else {
                        $size_txt .= 'width="'.$size[0].'px" height="'.$size[1].'px"';
                        }
                }
                return $size_txt;
        }
        
        function back_link ($needle, $back_txt, $else_txt, $exclusion = false) {
               $link_txt = '<a href="';
               if (preg_match ('/'.preg_replace('/\//','\/',HTTP_HOST.HTTP_BASE).'(.*?)'.preg_replace('/\//','\/',$needle).'(.*?)/', $_SERVER["HTTP_REFERER"]) AND $exclusion == false) {
               $link_txt .= $_SERVER["HTTP_REFERER"].'">'.$back_txt.'</a>';
               } else {
                $link_txt .= HTTP_HOST.HTTP_BASE.$needle.'">'.$else_txt.'</a>';
               }
               return $link_txt;
        }
        
        
?>