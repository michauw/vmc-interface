<?php
	

	class Language {
		function get_languages_subgroups_ids ($group_id, $depth=9999) {
			global $db;
			if ($depth>0) {
			$subgroups = array();
			$sql = 'SELECT `id` FROM `'.TAB_LANGUAGES_GROUPS.'` WHERE `parent_id` = "'.$group_id.'"';
			$res = $db->query($sql);
			
			if ($res->numRows()>0) {
				while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
					$subgroups[] = $row['id'];
					$subgroups = array_merge($subgroups,Language::get_languages_subgroups_ids($row['id'], ($depth-1)));
				}

				
			}
			return array_unique($subgroups);
			} else {
			return array();
			}
			
		}


		function get_languages_ids ($group_id) {
			global $db;
			
			$languages = array();
			
			
			$groups = Language::get_languages_subgroups_ids($group_id);
			$groups[] = $group_id;
						
			$sql = 'SELECT `id` FROM `'.TAB_LANGUAGES.'` WHERE `languages_groups_id` IN ('.implode(',',$groups).') AND `status` = "1" ORDER BY `languages_groups_id`, `name`';
		
			$res = $db->query($sql);
			
			if ($res->numRows()>0) {
				while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
					$languages[] = $row['id'];
				}
			} 		
						
			return $languages;
		}
		
		function get_languages_ids_abbr ($group_id) {
			global $db;
			
			$languages = array();
			
			
			$groups = Language::get_languages_subgroups_ids($group_id);
			$groups[] = $group_id;
						
			$sql = 'SELECT `id`, `abbr` FROM `'.TAB_LANGUAGES.'` WHERE `languages_groups_id` IN ('.implode(',',$groups).') AND `status` = "1" ORDER BY `languages_groups_id`, `name`';
		
			$res = $db->query($sql);
			
			if ($res->numRows()>0) {
				while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
					$languages[strtolower($row['abbr'])] = $row['id'];
				}
			} 		
						
			return $languages;
		}
		
		
		function get_language_group_name ($group_id) {
			global $db;
			
			$name = '';
			
			$sql = 'SELECT `name` FROM `'.TAB_LANGUAGES_GROUPS.'` WHERE `id` = "'.$group_id.'"';
			$res = $db->query($sql);
			
			if ($res->numRows()>0) {
				while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
					$name = $row['name'];
				}
			}
			
			return $name;
			
		}
		
		function get_language_group_abbr ($group_id) {
			global $db;
			
			$abbr = '';
			
			$sql = 'SELECT `abbr` FROM `'.TAB_LANGUAGES_GROUPS.'` WHERE `id` = "'.$group_id.'"';
			$res = $db->query($sql);
			
			if ($res->numRows()>0) {
				while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
					$abbr = $row['abbr'];
				}
			}
			
			return $abbr;
			
		}
		
		function get_language_name ($language_id) {
			global $db;
			
			$name = '';
			
			$sql = 'SELECT `name` FROM `'.TAB_LANGUAGES.'` WHERE `id` = "'.$language_id.'"';
			$res = $db->query($sql);
			
			if ($res->numRows()>0) {
				while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
					$name = $row['name'];
				}
			}
			
			return $name;
			
		}
		
		function get_language_abbr ($language_id) {
			global $db;
			
			$abbr = '';
			
			$sql = 'SELECT `abbr` FROM `'.TAB_LANGUAGES.'` WHERE `id` = "'.$language_id.'"';
			$res = $db->query($sql);
			
			if ($res->numRows()>0) {
				while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
					$abbr = $row['abbr'];
				}
			}
			
			return $abbr;
			
		}

	}
?>