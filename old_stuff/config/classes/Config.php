<?php
	class Config {
		
		function getIsLinkOnly ($linkid) {
			global $db;
			
			$sql = 'SELECT `linkonly` FROM `linksys_de` WHERE `link_id` = "'.$linkid.'"';
			$res = $db->query($sql);
			
			if ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
				if ($row['linkonly'] == 1) {
					return true;	
				} else {
					return false;
				}
			}
			
			return true;
			
		}
		
		function getFirstSublinkContent ($linkid) {
			global $db;
			
			$sql = 'SELECT `link_id` FROM `linksys_de` WHERE `parent_id` = "'.$linkid.'" AND `linkonly` = "0" ORDER BY `order` ASC LIMIT 1';
			$res = $db->query($sql);
			
			if ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
				return $row['link_id'];
			}
			
			return 0;
		}
		
		function getLinkidFromAction ($action) {
			global $db;
			
			$sql = 'SELECT `link_id` FROM `'.TAB_CONTENT_DE.'` WHERE `url_key` = "'.$action.'"';
			$res = $db->query($sql);
			
			if ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
				if (Config::getIsLinkOnly($row['link_id'])) {
					return Config::getFirstSublinkContent($row['link_id']);
				} else {
					return $row['link_id'];
				}
			}
			return ERRORDOC_ID;
		}
		
		function getContent ($action) {
			global $db;
			
			if ($action == "") {
				$sql = 'SELECT
							`link_id`
						FROM `'.TAB_CONTENT_DE.'`
						WHERE `parent_id` = "0"
						ORDER BY `order` LIMIT 1';
				$res = $db->query($sql);
				
				if ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
					$firstlinkid = $row['link_id'];
				} else {
					$firstlinkid = 0;
				}
				
				if (Config::getIsLinkOnly($firstlinkid)) {
					$linkid = Config::getFirstSublinkContent($firstlinkid);
				} else {
					$linkid = $firstlinkid;
				}
				
			} else {
				$linkid = Config::getLinkidFromAction($action);
			}
			$sql_content = 'SELECT
									a.`link_id`,
									a.`name`,
									a.`content`,
									UNIX_TIMESTAMP(a.`last_change`) as `last_change_date`,
									a.`script`,
									a.`tmpl_id`,
									b.`title`,
									b.`keywords`,
									b.`description`
								FROM `'.TAB_CONTENT_DE.'` AS a
								LEFT JOIN `'.TAB_CONTENT_META_DE.'` AS b
								 ON a.`link_id` = b.`link_id`
								WHERE a.`link_id` = "'.$linkid.'" AND a.`status` = "1"
								ORDER BY a.`order` LIMIT 1';
			return $db->query($sql_content); 
		}
		
		function getTerminTicker() {
			global $db;
			$termine = '';
			
			$sql = 'SELECT
						`name`,
						`event_id`,
						UNIX_TIMESTAMP(`event_date`) AS `date`
					FROM
						`events`
					WHERE
						UNIX_TIMESTAMP(`navig_show_to`) >= "'.time().'" AND
						UNIX_TIMESTAMP(`navig_show_from`) < "'.time().'" AND
						`status` = "1"
					ORDER BY `event_date` ASC';
			$res = $db->query($sql);
			
			if ($res->NumRows()>0) {
				$termine .= '<div class="title">Agenda</div>
							 <ul class="list">';
				
				while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
					$termine .= '<li>
									<strong>'.date("d.m.Y", $row['date']).'</strong><br />
									<a href="'.HTTP_HOST.HTTP_BASE.'de/events/event?event='.$row['event_id'].'">'.$row['name'].'</a>
								 </li>';
				}
				
				$termine .= '	</ul><br />
								';
				
			}
			return $termine;
		}
		
		function getNewsTicker() {
			global $db;
			$termine = '';
			
			$sql = 'SELECT
						`name`,
						`news_id`,
						UNIX_TIMESTAMP(`news_date`) AS `date`
					FROM
						`news`
					WHERE
						UNIX_TIMESTAMP(`news_date`)+(`duration`*86400*7) > "'.time().'" AND
						`status` = "1"
					ORDER BY `news_date` DESC, `last_change` DESC';
					
			$res = $db->query($sql);
			
			if ($res->NumRows()>0) {
				$termine .= '<div class="title">News</div>
							 <ul class="list">';
				
				while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
					$termine .= '<li>
									<strong>'.date("d.m.Y", $row['date']).'</strong><br />
									<a href="'.HTTP_HOST.HTTP_BASE.'de/news?id='.$row['news_id'].'">'.$row['name'].'</a>
								 </li>';
				}
				
				$termine .= '	</ul><br />
								';
				
			}
			return $termine;
		}
		
		
		
	}
?>