var sourceLang // enthält die aktuelle source language

function setSourceLang (f) {
	for (var j=0; j<f.primlang.length; j++){
		if (f.primlang[j].checked){
			sourceLang = f.primlang[j].name
		}
	}
}

function MakeArray(n){
   for (var i=0; i<n; i++)
      this[i] = 0
   this.length = n
}

function syncPrimlang(f,t){
	var selLangs = document.getElementsByName('langs[]');
	var selLangslaenge = selLangs.length;
	for(var i=0;i<selLangslaenge;i++) { 
		if(selLangs[i].value == t){
			if(selLangs[i].checked == false){ f.primlang[i].checked = false }
		}
	};
    chkForm(f)
}

function markLang(f){
	var selLangs = document.getElementsByName('langs[]');
	var selLangslaenge = selLangs.length;
	for(var i=0;i<selLangslaenge;i++) { 
			if(f.primlang[i].checked == true){ selLangs[i].checked = true }
	};
    chkForm(f)
}

function chkForm (f) {
	var ok1 = false
	var ok2 = false
	var selLangs = document.getElementsByName('langs[]');
	var selLangslaenge = selLangs.length;
	for (var j=0; j<f.primlang.length; j++){
		if (f.primlang[j].checked){	ok1 = true }
	}
	for(var i=0;i<selLangslaenge;i++) { 
		if((selLangs[i].checked)&&(!f.primlang[i].checked)){ ok2 = true }
	}	
//	if(ok1 && ok2){ document.forms["languages"].submit() } else {
	if(ok1 && ok2){ return true } else {
		alert('Please select a source language and at least one target language!')
		f.focus()
	}
}

function chkAll(t) {
	var seltexts = document.getElementsByName(t);
	var seltextslaenge = seltexts.length;
	for(var i=0;i<seltextslaenge;i++) { 
		seltexts[i].checked = true
	}
}

function chkNone(t) {
	var seltexts = document.getElementsByName(t);
	var seltextslaenge = seltexts.length;
	for(var i=0;i<seltextslaenge;i++) { 
		seltexts[i].checked = false
	}
}

function Go (select) {
	var wert = select.options[select.options.selectedIndex].value;
	if (wert == "leer") {
    	select.form.reset();
	    parent.frames["kwic"].focus();
    	return;
	} else {
		parent.frames["kwic"].location.href = wert;
		select.form.reset();
		parent.frames["kwic"].focus();
    }
}

// Extract query from CQPBox and save it to the outQuery (outQuery will be send to the server
function qcpQuery() {
	document.getElementById("outQueryCQP").value=document.getElementById("qcp").value;
}
// Remove spaces at the ends of string
function removeSpaces(str)
{
  var bufStr;
  var begin = 0;
  var end = str.length-1;
  while (str[begin] == " ") begin++;
  while (str[end] == " ") end--;
  return str.substring(begin, end+1);	
}

function replaceAndEscapeChrs(value){
	value = value.replace(/\(/g, "\\(");
	value = value.replace(/\)/g, "\\)");
	value = value.replace(/\[/g, "\\[");
	value = value.replace(/\]/g, "\\]");
	value = value.replace(/\{/g, "\\}");
	value = value.replace(/\{/g, "\\}");
	value = value.replace(/\+/g, "\\+");
	value = value.replace(/\_/g, "\\_");
	value = value.replace(/\-/g, "\\-");
	value = value.replace(/\*/g, "\\*");
	value = value.replace(/\./g, "\\.");
	value = value.replace(/\:/g, "\\:");
	value = value.replace(/\;/g, "\\;");
	value = value.replace(/\,/g, "\\,");
	value = value.replace(/\?/g, "\\?");
	value = value.replace(/\!/g, "\\!");
	return value;
}
// Extract query from ExactQueryBox and save it to the outQuery (outQuery will be sent to the server)
function exactPhraseQuery() 
{
	var quellText = removeSpaces(document.getElementById("exactQ").value);
	document.getElementById("exactQ").value = quellText;
	quellText = replaceAndEscapeChrs(quellText);
	var resultText = "\"";
       var i = 0;
	// make from 'hallo you' query : '"hallo" "you"'
       while (i < quellText.length)
       {
		var ch = quellText.charAt(i);
		if (ch == ' ')
		{
			resultText+="\"";
		}
		resultText+=ch;
		if (ch == ' ')
		{
			resultText+="\"";
		}
		i++;
       }
	resultText+="\"";
	// extract case-sensitive checkbox from exactphrasesearch-box
	var checkBoxes = document.getElementsByName("checkBox0");
	var caseSensitive = checkBoxes[0].checked;
	// check if 'case sensitive' seted
	if (!caseSensitive)
		resultText= resultText + "%c";
	document.getElementById("outQueryExact").value=resultText;
	//alert(document.getElementById("outQueryExact").value);
}

// Extract query from AdvancedSearchBox and save it to the outQuery (outQuery will be sent to the server)
function boundQuery(){
	var begin = document.getElementById("bQBegin").value.replace(/ /g, "");
	document.getElementById("bQBegin").value = begin;
	begin = replaceAndEscapeChrs(begin);
	var checkBoxes = document.getElementsByName("checkBox1");
	
	// extract case-sensitive checkbox from advancedsearch-box
	var caseSensitive = checkBoxes[2].checked;
	if (checkBoxes[0].checked)
		begin = begin + ".*";
	if (checkBoxes[1].checked)
		begin = ".*" + begin;
	var query = "\"" + begin + "\"";

	// Read queries from all boxes of AdvancedSearchBox
	for (var i=2; i <= boxN; i++)
		if (document.getElementById("bQEnd" + i).value!="")
		{
			var from = 0;
			var to = 0;
			var fromId = "bQFrom" + i;
			var toId = "bQTo" + i;
			var endId = "bQEnd" + i;
			var checkBoxId = "checkBox" + i;
			var end = document.getElementById(endId).value.replace(/ /g, "");
			document.getElementById(endId).value = end;
			end = replaceAndEscapeChrs(end);
			checkBoxes = document.getElementsByName(checkBoxId);
			if (checkBoxes[0].checked)
				end = end + ".*";
			if (checkBoxes[1].checked)
				end = ".*" + end ;
			if ((document.getElementById(fromId).value!="")&&(document.getElementById(toId).value!=""))
			{
				from = document.getElementById(fromId).value;
				to = document.getElementById(toId).value;
			} else if ((document.getElementById(fromId).value=="")&&(document.getElementById(toId).value==""))
			{
				document.getElementById(fromId).value=1;
				document.getElementById(toId).value=1;
				from = document.getElementById(fromId).value;
				to = document.getElementById(toId).value;
			} else if ((document.getElementById(fromId).value!="")&&(document.getElementById(toId).value==""))
			{
				document.getElementById(toId).value=document.getElementById(fromId).value;
				from = document.getElementById(fromId).value;
				to = document.getElementById(toId).value;
			} else if ((document.getElementById(fromId).value=="")&&(document.getElementById(toId).value!=""))
			{
				document.getElementById(fromId).value=document.getElementById(toId).value;
				from = document.getElementById(fromId).value;
				to = document.getElementById(toId).value;
			}
			query+="[]{"+from+","+to+"}" + "\"" + end + "\"";

		}
	// check if 'case sensitive' set
	if (!caseSensitive)
		query= query + "%c";
	document.getElementById("outQueryAdvanced").value=query;
	//alert(document.getElementById("outQueryAdvanced").value);
}

// This function makes Advancedserach visible/unvisible by switching plus-image
function extendedSearch(){
	if (document.getElementById("box2").style.visibility!="visible")
	{
		for (var i=2; i <= boxN; i++)
		{
			var bf = "box" + i;
			document.getElementById(bf).style.visibility="visible";
		}
		document.getElementById("bigBox2").style.visibility="visible";
		document.getElementById("boundBtn").style.visibility="visible";
		document.getElementById("boundBtnXml").style.visibility="visible";
		document.getElementById("info2").style.visibility="visible";
		document.getElementById("info3").style.visibility="visible";
		document.getElementById("boxQCP").style.visibility="visible";
		document.getElementById("bigBoxQCP").style.visibility="visible";
		document.getElementById("qcpBtn").style.visibility="visible";
		document.getElementById("qcpBtnXml").style.visibility="visible";
		document.getElementById("up").style.visibility="visible";
		document.getElementById("down").style.visibility="visible";
		document.getElementById("l1").innerHTML =  "Exact Phrase:";
	}
	else
	{
		for (var i=2; i <= boxN; i++)
		{
			var bf = "box" + i;
			document.getElementById(bf).style.visibility="collapse";
		}
		document.getElementById("bigBox2").style.visibility="collapse";
		document.getElementById("boundBtn").style.visibility="collapse";
		document.getElementById("boundBtnXml").style.visibility="collapse";
		document.getElementById("info2").style.visibility="collapse";
		document.getElementById("info3").style.visibility="collapse";
		document.getElementById("boxQCP").style.visibility="collapse";
		document.getElementById("bigBoxQCP").style.visibility="collapse";
		document.getElementById("qcpBtn").style.visibility="collapse";
		document.getElementById("qcpBtnXml").style.visibility="collapse";
		document.getElementById("up").style.visibility="collapse";
		document.getElementById("down").style.visibility="collapse";
		document.getElementById("l1").innerHTML =  "";
	}
}

// Add a new Word and bound-inputs to advancedsearch
function openBox()
{
	boxN++;
	var id = boxN;
	var boxId = "box" + id;
	var parentBoxId = "#box" + (id - 1);
	var textId = "bQEnd" + id;
	var htmlString =       '<br id="br' + id + '"/>'
				+ '<div id="box' + id + '" style="width:100%; float: left; visibility:visible">'
				+ '<div  class="gradient-opacity"; style="float:left; width:570px; height:34px;">'
				+ '	<label style="margin-left:250px; width:140px; font-weight: bold; font-family:\'EB Garamond\', serif; font-size:15px;">Tokens in between:</label>'
				+ '	<label style="font-weight: lighter; font-family:\'EB Garamond\', serif; font-size:12px; width:35px; margin-left:10px;" > from </label>'
				+ '	<input type="text" id="bQFrom' + id + '" style="font-size:12px; font-family:Garamond, serif; width:30px" name="bound_begin" value="0"/>'
				+ '	<label style="font-weight: lighter; font-family:\'EB Garamond\', serif; font-size:12px; width:30px; text-align:center">to</label>'
				+ '	<input type="text" id="bQTo' + id + '" style="font-size:12px; font-family:Garamond, serif; width:30px" name="bound_end" value="0"/>'
				+ '</div>'
				+ '<div  class="gradient-opacity2"; style="float:left; width:30px; height:34px;"></div>'
				+ '<br />'
				+ '	<label style="float-left;  font-weight: bold; font-family:\'EB Garamond\', serif; font-size:15px; margin-left:10px; width:auto;"> ' + id + '. Word</label>'
				+ '	<input style="width:522px; margin-left:5px; float:left"; type="text" id="bQEnd' + id + '" onkeyup=getSuggestion("bQEnd' + id + '") name="query_end" class="keyboardInput"/>'
				+ '<br />'
				+ '<br />'
				+ '	<input type="checkbox" style="margin-left:10px; width:10px;" name="checkBox' + id + '" value="yes1"/>'
				+ '	<label style="font-weight: lighter; font-family:\'EB Garamond\', serif; font-size:12px; margin-left:10px; width:70px;"> begins with</label>'
				+ '	<input type="checkbox" style=" width:10px;" name="checkBox' + id + '" value="yes2"/>'
				+ '	<label style="font-weight: lighter; font-family:\'EB Garamond\', serif; font-size:12px; margin-left:10px; width:65px;"> ends with</label>'
				+ '</div>';
	$(parentBoxId).after(htmlString);
	VKI_attach(document.getElementById(textId));
}

// Remove last word from advanced search
function closeBox(){
	var boxId = "#box" + boxN;
	var brId = "#br" + boxN;
  	if (boxN  > 2)
  	{
		$(boxId).remove();
		$(brId).remove();
		boxN--;
  	}
}

// Autocomplete query will be saved here
var globalQuery="xx";


// Extract query from input-field (or other given string)
// Example: string str "make it" functions return query-string '"m.*"[]{0,0}"i.*"'
function extractQuery(str)
{
  var query = "";
  var i = 0;
  
  // returns empty string if str is empty or begins with space or tab 
  if (str.length < 1 || str[i]==" " || str[i]=="\t") return ""; 
  // extract query character or the first word 
  query = '"' + str[i] + '.*"';
  i++;
  while(i < str.length)
  {
	if (str[i]==" ")
	{
		i++;
		if (i >= str.length || str[i]==" " || str[i]=="\t") return query;
		query+= '[]{0,0}"' + str[i] + '.*"';
	}
	i++;
  }
  return query;	 
}

// Ajax-connector between autocomplete.php and UserInterface
function getSuggestion(id)
{
  var query = extractQuery(document.getElementById(id).value);
  if (query.length > 0 && query != globalQuery)
  {
	globalQuery = query;
	//alert(globalQuery);
 	$.ajax({
		url : 'autocomplete.php',
		dataType : 'json',
		data: {
			query : globalQuery
		}, 
		success: loadData});
  }
}

// List of suggestions/words/phrases, wich was sent from server
var dataAutocomplete;

// Load words and phrases, wich we have got from server (autocomplete.php) to jquery-autocomplete
function loadData(data, textStatus, jqXHR)
{ 
    console.log ('auto:', data);
	dataAutocomplete = data;
	$( ".keyboardInput" ).autocomplete({
			source: dataAutocomplete
		});
	$( ".keyboardInput" ).autocomplete( "option", "minLength", 1);
	$( ".keyboardInput" ).autocomplete( "option", "delay", 0);

}