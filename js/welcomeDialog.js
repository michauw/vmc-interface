$(document).ready(function() {
	document.getElementById('title').innerHTML='\u0412\u0435\u043B\u0438\u043A\u0438\u0435\ \u041C\u0438\u043D\u0435\u0438\ \u0427\u0435\u0442\u044C\u0438\ ';
	var screenWidth = $(window).width();
	var screenHeight = $(window).height() - 167;
	var infoWidth = screenWidth - 70;
	var imageWidth= screenWidth;
	imageWidth*=0.5;
	imageWidth+="px";
	var imageMarg= screenWidth;
	imageMarg*=0.2;
	imageMarg+="px";
	//alert(document.getElementById('searchFields').style.width + " " + infoWidth);
	//document.getElementById('infoBigBox').style.width=infoWidth + "px";
	document.getElementById('infoBigBox').style.height=screenHeight + "px";
	
	// Welcom dialog for more information you must search in google for "dialog jquery"
	var strWelcom = '</br>'//'<h3>Welcome to the <b>VM\u010C\</b> Corpus!<h3/> <br /> <br />'
		+'The <b>VM\u010C\</b> (<em style="font-family:slavicfont; font-size:18px">\u0412\u0435\u043B\u0438\u043A\u0438\u0435\ \u041C\u0438\u043D\u0435\u0438\ \u0427\u0435\u0442\u044C\u0438\</em>) represent one of the most important monuments of Russian Church Slavonic literature compiled by Macarius, Metropolitan of Moscow and his scribes in the 16<sup>th</sup> century. <br /> <br />'
		+'The VM\u010C\ corpus is being developed as part of <a href="http://www.slavistik.uni-freiburg.de/forschung/lprojekte/russisches-kirchenslavisch-im-15.-und-16.-jahrhundert-zur-in-variabilitaet-seiner-normen-am-material-des-kommentierten-apostolos"> a project funded by the DFG </a> of the Department for Slavonic Studies at the University of Freiburg, Germany. This project continues <a href="http://forschdb.verwaltung.uni-freiburg.de/servuni/forschdbuni.recherche0?xmldokumentart=Projekt&lfdnr=2853&sprache=D&Layout=uni&Ausgabeart=bs&Rahmen=1&Variante=3">the publication</a> of a printed edition of the yet unpublished portions of the VM\u010C\. For more information on these and other projects, please visit the website of <a href="http://www.slavistik.uni-freiburg.de/forschung">the Department for Slavic Studies</a>. <br /> <br />'
              +'The VM\u010C\ are organized according to months and days of the calendar year. On this website you can access the Acts of the Apostles with patristic commentaries, Pauline epistles, and the Ecumenical letters, all of which relate to the 30<sup>th</sup> of June (Weiher et al. 1997; Rabus et. al 2012). <br /> <br />'
	       +'<h3> VM\u010C\ development team: <h3/> <br />'
              +'- Dr. Ruprecht von Waldenfels (overall concept and project supervisor) <br />'
              +'- Dr. Achim Rabus (consultation and specifications) <br />'
              +'- Simon Skilevic, B.A. (programming and design) <br />'
              +'- Stefan Savi\u0107\, M.A. (texts and design) <br /> <br />'
              +'<h3> Bibliography: <h3/> <br />'
              +'Rabus, A., Savi\u0107\, S., Waldenfels, R. v. 2012. Towards an electronic corpus of the Velikie Minei \u010C\et\u0027\i. In: <em>Rediscovery: Bulgarian Codex Suprasliensis of the 10<sup>th</sup> century</em>. Sofia: Iztok Zapad. <br /> <br />'
              +'Weiher, E. et al. 1997, 1998, 2001, 2007, 2009. <em>Die Gro\u00DF\en Lesemen\u00E4\en des Metropoliten Makarij</em>. 1-31 M\u00e4\u0072\z, 1-23 Mai. Freiburg i. Breisgau.: Weiher. <br /> <br /> ' ;
        document.getElementById('infoBox').style.height=screenHeight - 50 + "px";
	$("#infoBox").html(strWelcom);
});