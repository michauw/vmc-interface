// This script will be executed exactly after load of site
$(document).ready(function() {
	document.getElementById('title').innerHTML='\u0412\u0435\u043B\u0438\u043A\u0438\u0435\ \u041C\u0438\u043D\u0435\u0438\ \u0427\u0435\u0442\u044C\u0438\ ';
	var screenWidth = $(window).width();
	var screenHeight = $(window).height() - 167;
	var bf = document.getElementById('searchFields').style.width.replace("px","");
	var infoWidth = screenWidth - Number(bf) - 110;
	var imageWidth= screenWidth - Number(bf);
	imageWidth*=0.7;
	imageWidth+="px";
	var imageMarg= screenWidth - Number(bf);
	imageMarg*=0.07;
	imageMarg+="px";
	//alert(document.getElementById('searchFields').style.width + " " + infoWidth);
	document.getElementById('infoBigBox').style.width=infoWidth + "px";
	document.getElementById('infoBigBox').style.height=screenHeight + "px";
	
	// Default number of input-tags by advanced search
	boxN=2;
	// Welcom dialog for more information you must search in google for "dialog jquery"
	var strWelcom = '</br>'//'<h3>How do I use the corpus?<h3/> <br /> <br />'		
              +'<h3>How do I use the corpus?<h3/> <br /> <br />'
              +'Please note that there is a great orthographic variation throughout the whole corpus. When looking up a word, be aware of the following: <br /> <br />'
		+'1. Most frequent words are spelled as abbreviations with a <b style="font-family:slavicfont; font-size:18px">titlo</b> above, such as <b style="font-family:slavicfont; font-size:18px">\u0431\u0483\u044A\</b> instead of <b style="font-family:slavicfont; font-size:18px">\u0431\u043E\u0433\u044A\</b>. <br /> <br />'
		+'2. Many words contain superscript characters <nobr> (for example: <b style="font-family:slavicfont; font-size:18px">\u043F\u0440\u0435\u2DE3\u043B\u0430\u0433\u0430\u0454\u2DEE\</b> instead of <b style="font-family:slavicfont; font-size:18px">\u043F\u0440\u0435\u0434\u043B\u0430\u0433\u0430\u0435\u0442\u044A\</b>) </nobr> <br /> <br />'
		+'3. Certain letters are interchangeable with other letters, such as: <br /> <br />' 
		+'<b style="font-family:slavicfont; font-size:18px">\u043E\</b> with <b>\u0461\</b>,<b style="font-family:slavicfont; font-size:18px">\uA64D\</b> and the corresponding superscripts; <br /> <br />'
		+'<b style="font-family:slavicfont; font-size:18px">\u0435\</b> with <b>\u0454\</b>, <b style="font-family:slavicfont; font-size:18px">\u0463\</b> and <b style="font-family:slavicfont; font-size:18px">\u0465\</b>, in some cases even with <b style="font-family:slavicfont; font-size:18px">\u0467\</b>, <b style="font-family:slavicfont; font-size:18px">\u0430\</b> and <b style="font-family:slavicfont; font-size:18px">\u0438\</b> and the corresponding superscripts. <br /> <br />'
              +'<em>Searching the corpus:</em> <br /> <br />'	       
              +'In order to facilitate searching and to avoid problems with the various spellings, we are using <em>Autocomplete</em>, which automatically displays all the words present in the corpus that match the first two or more characters you type in the search box. <br /> <br />' 
              +'<img style="border-style:solid; border-width:1px; border-color:#663300; width:'+imageWidth+'; margin-left:'+imageMarg+';" src="images/autocomplete.png"> <br /> <br />' 
              +'<em> Search boxes: </em> <br /> <br />'
              +'Since we are trying to accommodate a wide range of user profiles with suitable searching possibilities, we have also introduced three different types of search boxes. In the <em>Simple Search</em> box you can search for a particular word form. The <em>Advanced Search</em> box enables you to look up a word form or a part of a word. In addition, your query may include more than one word at a time and you may determine the distance between them. The <em>Complex Search</em> box is meant for users who are familiar with CQP syntax. <br /> <br />'
              +'For more information on how to use each search box, click on <img src="images/info.png">. <br /> <br />' 
		+'<em>Virtual keyboard:</em> <br /> <br />'
              +'Enter a word you want to look up in the search box. If you want to use characters not available on your keyboard (such as <b style="font-family:slavicfont; font-size:18px">\u0463\</b>, <b style="font-family:slavicfont; font-size:18px">\u0467\</b>, <b style="font-family:slavicfont; font-size:18px">\u0461\</b>, the superscript characters <nobr> <b style="font-family:slavicfont; font-size:18px">     [ \u2DE0\ ,   \u2DE6\ ]</b> </nobr>, the titlo <nobr> (  [ <bstyle="font-family:slavicfont; font-size:18px">\u0483\</b> ]  ) </nobr> etc.), click <img src="images/tastatur.png"> on the right side of the search box. All characters appearing in the VM\u010C\ are arranged into three groups: <br /> <br />' 
              +'1) <em>Cyrillic</em> (characters appearing in the VM\u010C\ which are still used in the modern Russian Cyrillic alphabet), <br /> <br />' 
              +'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/cyrillic.png"> <br /> <br />'
              +'2) <em>Special characters</em> (archaic Old Church Slavonic characters), <br /> <br />'
              +'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/spec-charact.png"> <br /> <br />'
              +'and <br /> <br />'
              +'3) <em>Superscript characters</em> (small characters appearing on top of other characters). <br /> <br />' 
              +'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/superscr-charact.png"> <br /> <br />'
              +'The <em>special characters</em> are set as the default. You can easily select another character set by clicking the upper left corner of the virtual keyboard, where the currently selected character set always is indicated: <br /> <br />' 
              +'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/special-characters-redframe.png"><br /> <br />' 
		+'<em> Results: </em> <br /> <br />'
              +'When you type in a word you want to look up, you can choose to have the results displayed in a new tab by clicking on <img src="images/search-button.png">. If you would like the results to be displayed as an XML file, click on <img src="images/export.png">. <br /> <br />'
              +' If you look up the word <b style="font-family:slavicfont; font-size:18px">\u0432\u0434\u043E\u0432\u0430\u2DE8\</b> (<b style="font-family:slavicfont; font-size:18px">\u0432\u0434\u043E\u0432\u0430\u043C\</b>) and click on <img src="images/search-button.png"> <br /> <br />'              
              +'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/vdovam.png"> <br /> <br />'
              +'the search results page opens. The searched word is marked in red: <br /> <br />' 

		// Style einstellungen f�r alle Bilder �bernehmen. Mit widht kann man Gr��e des Bildes �ndern.

              +'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';" src="images/vdovam-wordframes.png"> <br /> <br />'
              +'In addition the results page displays the following information on the location of the searched word or phrase in the original text: <br /> <br />'
              +' - the ordinal number of the searched word (or phrase): <br /> <br />'
              +'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/vdovam-tokennr.png"> <br /> <br />'
              +' - the page number in the original text: <br /> <br />'
              +'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/vdovam-pagenr.png"> <br /> <br />'
              +' - the column number (as each page is divided into two columns): <br /> <br />'   
              +'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/vdovam-columnnr.png"> <br /> <br />'
              +' - the line number: <br /> <br />' 
              +'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/vdovam-linenr.png"> <br /> <br />'
              +'The number of results matching your search is displayed at the bottom: <br /> <br />'
              +'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/vdovam-hits.png"> <br /> <br />'
              // Informationdialog for exactphrase box
	var dialog1 = '<h3>Simple Search<h3/> <br />'

+'Enter a word form to see all the contexts in which it appears. <br /> <br />'
+'I. If you type in <b style="font-family:slavicfont; font-size:18px">\u043B\u0463\u0442a</b>: <br /> <br />' 
+'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/LETA-searchbar.png"> <br /> <br />'
+'all instances of <b style="font-family:slavicfont; font-size:18px">\u043B\u0463\u0442a</b> will be displayed, but NOT <b style="font-family:slavicfont; font-size:18px">\u043B\u0463\u0442\u044A</b>, <b style="font-family:slavicfont; font-size:18px">\u043B\u0463\u0442o</b>, <b style="font-family:slavicfont; font-size:18px">\u043B\u0463\u0442\u0461</b>, <b style="font-family:slavicfont; font-size:18px">\u043B\u0463\u0442\u0463\u0445</b> etc. <br /> <br />'
+'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/LETA.png"> <br /> <br />'
+'II. If you enter more than one word, such as <b style="font-family:slavicfont; font-size:18px">\u043A\u0442\u043E\ \u0440\u0435\u0447\u0435\</b>:  <br /> <br />'
+'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/kto-reche-searchbox.png"> <br /> <br />'
+'all instances of <b style="font-family:slavicfont; font-size:18px">\u043A\u0442\u043E\ \u0440\u0435\u0447\u0435\</b> will be displayed, but NOT: <br />' 
+'<b style="font-family:slavicfont; font-size:18px">\u0440\u0435\u0447\u0435\ \u043A\u0442\u043E\</b> <br />' 
+'<b style="font-family:slavicfont; font-size:18px">\u043A\u0442\u043E\ , \u0440\u0435\u0447\u0435\</b> <br />'
+'<b style="font-family:slavicfont; font-size:18px">\u0440\u0435\u0447\u0435\ \u0479\u0431\u043E\ , \u043A\u0442\u043E\</b> etc. <br /> <br />'
+'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/kto-reche.png"> <br /> <br />'
	// Informationdialog for advanced search box (bigest box)
	var dialog2 ='<h3>Advanced Search<h3/><br />'

+'Enter a word form to see all the contexts in which it appears. <br /> <br />'
+'I. If you type in <b style="font-family:slavicfont; font-size:18px">\u043B\u0463\u0442a</b>: <br /> <br />' 
+'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/LETA-searchbar.png"> <br /> <br />'
+'all instances of <b style="font-family:slavicfont; font-size:18px">\u043B\u0463\u0442a</b> will be displayed, but NOT <b style="font-family:slavicfont; font-size:18px">\u043B\u0463\u0442\u044A</b>, <b style="font-family:slavicfont; font-size:18px">\u043B\u0463\u0442o</b>, <b style="font-family:slavicfont; font-size:18px">\u043B\u0463\u0442\u0461</b>, <b style="font-family:slavicfont; font-size:18px">\u043B\u0463\u0442\u0463\u0445</b> etc. <br /> <br />'
+'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/LETA.png"> <br /> <br />'
+'II. By checking off <em>Begins with</em> or <em>Ends with</em>, you can also specify which characters the searched word starts or ends with. For example, if you type in <b style="font-family:slavicfont; font-size:18px">\u0432\u0438\u0434</b> and check off <em>Begins with</em> <br /> <br />'
+'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/VID-beginswith.png"> <br /> <br />'
+'the results will include instances such as <b style="font-family:slavicfont; font-size:18px">\u0432\u0438\u0434\u044A</b>, <b style="font-family:slavicfont; font-size:18px">\u0432\u0438\u0434\u0479</b>, <b style="font-family:slavicfont; font-size:18px">\u0432\u0438\u0434\u0463\u0442\u0438</b>, <b style="font-family:slavicfont; font-size:18px">\u0432\u0438\u0434\u0438\u0442\u0435</b>, <b style="font-family:slavicfont; font-size:18px">\u0432\u0438\u0434\u0438\u0448\u0438</b>, <b style="font-family:slavicfont; font-size:18px">\u0432\u0438\u0434\u0467\u0448\u0435</b> etc. <br /> <br />'
+'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/vid-results.png"> <br /> <br />'
+'By clicking <b>Ends with</b> all words ending with the characters you typed in will appear. \n'
+'For example, if you enter <b style="font-family:slavicfont; font-size:18px">\u043E\u043C\u044C</b> and check off <em>Ends with</em>: <br /> <br />'
+'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/omj-endswith.png"> <br /> <br />'
+'the reults will include instances such as <b style="font-family:slavicfont; font-size:18px">\u0454\u0434\u0438\u043D\u043E\u043C\u044C</b>, <b style="font-family:slavicfont; font-size:18px">\u0441\u043B\u043E\u0432\u043E\u043C\u044C</b>, <b style="font-family:slavicfont; font-size:18px">\u0431\u043E\u0433\u043E\u043C\u044C</b> etc. <br /> <br />' 
+'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/om-results.png"> <br /> <br />'
+'III. You can also search for more than one word (or punctuation mark) at the same time. Just enter each word into separate search boxes. <br /> <br />'
+'Specify how many words (or punctuation marks) may occur between the searched words (in the fields <em>from</em> and <em>to</em>). <br /> <br />'
+'For example, if you want to search for <b style="font-family:slavicfont; font-size:18px">\u0440\u0435\u0447\u0435\</b> and <b style="font-family:slavicfont; font-size:18px">\u0430\u0449\u0435\</b>, type <b style="font-family:slavicfont; font-size:18px">\u0440\u0435\u0447\u0435\</b> into the upper box (<em>1. Word</em>) and <b style="font-family:slavicfont; font-size:18px">\u0430\u0449\u0435\</b> into the lower search box (<em>2. Word</em>). If you want to see only the instances of <b style="font-family:slavicfont; font-size:18px">\u0430\u0449\u0435\</b> immediately following <b style="font-family:slavicfont; font-size:18px">\u0440\u0435\u0447\u0435\</b> (<b style="font-family:slavicfont; font-size:18px">\u0440\u0435\u0447\u0435\ \u0430\u0449\u0435\</b>), leave <em>0</em> in the boxes <em>to</em> and <em>from</em>: <br /><br />'
+'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/reche-asche-00.png"> <br /> <br />'
+'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/reche-ashte-0-results.png"> <br /> <br />'
+'If you specify the distance between the two searched words as 0 to 5 words (from:0; to:5) the results would include instances such as:<br />'
+'-<b style="font-family:slavicfont; font-size:18px">\u0440\u0435\u0447\u0435\ . \u0447\u0442\u043E\ \u0479\u0431\u043E\ , \u0430\u0449\u0435\</b> (distance: 4 words)<br />'
+'-<b style="font-family:slavicfont; font-size:18px">\u0440\u0435\u0447\u0435\ \u0479\u0431\u043E\ \u0442\u0438\u043C\u043E\u0473\u0435\u0438\ . \u0430\u0449\u0435\</b> (distance: 3 words)<br />'
+'-<b style="font-family:slavicfont; font-size:18px">\u0440\u0435\u0447\u0435\ \u0431\u0461\ , \u0430\u0449\u0435\</b> (distance: 2 words). <br /> <br />'
+'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/reche-asche-05.png"> <br /> <br />'
+'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/reche-ashte-0-5-results.png"> <br /> <br />'
+'You can also add search boxes by clicking <img src="images/down.png"> <br /> <br />' 
+'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/two-to-three-searchbars.png"> <br /> <br />'
+'or remove them by clicking <img src="images/up.png">. <br /><br />'
+'<img style="border-style:solid; border-width:1px; border-color:#663300;width:'+imageWidth+'; margin-left:'+imageMarg+';"src="images/three-to-two-searchbars.png"> <br /> <br />';
	// Informationdialog for CQP box
	var dialog3 = '<h3>Complex Search <h3/><br />'

+'Use CQP Syntax in your query. Every word has to start and end with quotation marks. <br /><br />'
+'CQP offers some additional solutions to problems of spelling variation, such as: <br />'
+'- Replacing any character by a full stop ( <em>.</em> ): <br />'
+'   <b style="font-family:slavicfont; font-size:18px">"\u0432\.\u0434\.\u0442\u0438\"</b> will include results like <b style="font-family:slavicfont; font-size:18px">\u0432\u0438\u0434\u0463\u0442\u0438\</b>, <b style="font-family:slavicfont; font-size:18px">\u0432\u0463\u0434\u0463\u0442\u0438\</b>, <b style="font-family:slavicfont; font-size:18px">\u0432\u0463\u0434\u0430\u0442\u0438\</b>; <br />' 
+'   <b style="font-family:slavicfont; font-size:18px">"\u0434\u043E\u0431\u0440\."</b>:  <b style="font-family:slavicfont; font-size:18px">\u0434\u043E\u0431\u0440\u044B\</b>, <b style="font-family:slavicfont; font-size:18px">\u0434\u043E\u0431\u0440\u0430\</b>, <b style="font-family:slavicfont; font-size:18px">\u0434\u043E\u0431\u0440\u043E\</b>, <b style="font-family:slavicfont; font-size:18px">\u0434\u043E\u0431\u0440\u044A\</b>, <b style="font-family:slavicfont; font-size:18px">\u0434\u043E\u0431\u0440\uA64B\</b>, <b style="font-family:slavicfont; font-size:18px">\u0434\u043E\u0431\u0440\u0463\</b>, <b style="font-family:slavicfont; font-size:18px">\u0434\u043E\u0431\u0440\u2DFA\</b>; <br />'
+'   <b style="font-family:slavicfont; font-size:18px">"\u0434\u043E\u0431\u0440\u044B\.."</b>: <b style="font-family:slavicfont; font-size:18px">\u0434\u043E\u0431\u0440\u044B\u043C\u0438\</b>, <b style="font-family:slavicfont; font-size:18px">\u0434\u043E\u0431\u0440\u044B\u043C\u044C\</b>, <b style="font-family:slavicfont; font-size:18px">\u0434\u043E\u0431\u0440\u044B\u044C\u2DE8\</b>, <b style="font-family:slavicfont; font-size:18px">\u0434\u043E\u0431\u0440\u044B\u044C\u2DEF\</b>; <br />'
+'- Replacing a character sequence by <em>.*</em>: <br />'
+'   <b style="font-family:slavicfont; font-size:18px">"\u0441\.\u0442\u0432\.*"</b>: <b style="font-family:slavicfont; font-size:18px">"\u0441\u044A\u0442\u0432\u043E\u0440\u0438\u0442\u0438\</b>, <b style="font-family:slavicfont; font-size:18px">"\u0441\u044A\u0442\u0432\u043E\u0440\u0438\u2DEF\</b>, <b style="font-family:slavicfont; font-size:18px">\u0441\u044A\u0442\u0432\u043E\u0440\u0435\u043D\</b>, <b style="font-family:slavicfont; font-size:18px">\u0441\u043E\u0442\u0432\u043E\u0440\u0438\u0445\u043E\u043C\u044C\</b> etc. <br />'
+'- Using the "or-operation" <em>(|)</em>: <br />'
+'  <b style="font-family:slavicfont; font-size:18px">"\u0434\(.\u043B\|\u043B\.)\u0433\u044A\"</b>: <b style="font-family:slavicfont; font-size:18px">\u0434\u043B\u044A\u0433\u044A\</b> and <b style="font-family:slavicfont; font-size:18px">\u0434\u043E\u043B\u0433\u044A\</b>.'
	// Open dialog after start
	$("#infoBox").html(strWelcom);
	document.getElementById('infoBox').style.height=Number(document.getElementById('infoBigBox').style.height.replace("px","") - 40) + "px";
	//$welcomDilog.parent().addClass('shadow');
	//$dialog1.parent().addClass('shadow');
	//$dialog2.parent().addClass('shadow');
	//$dialog3.parent().addClass('shadow');

	// Add open events-functions for dialogs. If one dialog will be opended, the other dialogs will be closed
	$('#logo').click(function() {
		$("#infoBox").html(strWelcom);
		document.getElementById('infoBox').style.height=Number(document.getElementById('infoBigBox').style.height.replace("px","") - 40) + "px";
		return false;
	});

	// Add open events-functions for dialogs. If one dialog will be opended, the other dialogs will be closed
	$('#info1').click(function() {
		$("#infoBox").html(dialog1);
		document.getElementById('infoBox').style.height=Number(document.getElementById('infoBigBox').style.height.replace("px","") - 40) + "px";
		return false;
	});
	$('#info2').click(function() {		
		$("#infoBox").html(dialog2);
		document.getElementById('infoBox').style.height=Number(document.getElementById('infoBigBox').style.height.replace("px","") - 40) + "px";
		// prevent the default action, e.g., following a link
		return false;
	});
	$('#info3').click(function() {
		$("#infoBox").html(dialog3);
		document.getElementById('infoBox').style.height=Number(document.getElementById('infoBigBox').style.height.replace("px","") - 40) + "px";
		// prevent the default action, e.g., following a link
		return false;
	});

});

