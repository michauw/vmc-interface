<?php
//ini_set("session.use_cookies","1");

/**
 * 
 * 
 * Created on 01.03.2007
 * by Roland Meyer
 */

class converter {
	/**
	 * @class converter
	 * @author Roland Meyer  
	 */

	/**  
	 * input encoding
	 * @var string $ein
	 */
	var $ein;

	/**  
	 * output encoding
	 * @var string $aus
	 */
	var $aus;

	/**
	 * constructor 
	 * @method converter
	 * @param string ein
	 * @param string aus
	 */
	function converter($ein, $aus) {
		$this->ein = $ein;
		$this->aus = $aus;
	}

	/**
	 *  converts a string from $ein to $aus  
	 *  @param string chars 
	 */
	function code($zeichen) {
		$pipes = array();
		$descriptorspec = array(	0 => array("pipe", "r"), // stdin is a pipe that the child will read from
									1 => array("pipe", "w"), // stdout is a pipe that the child will write to
						);
		if($this->ein != $this->aus){
			$cmd = 'iconv -c -f ' . $this->ein . ' -t ' . $this->aus;
		} else {
			$cmd = 'cat';
		};
		$process = proc_open($cmd, $descriptorspec, $pipes);
		if (is_resource($process)) {
			fwrite($pipes[0], trim($zeichen));
			fclose($pipes[0]);
			$zeichen = fread($pipes[1], 999);
			fclose($pipes[1]);
			proc_close($process);
		}
		return $zeichen;
	}
}

/**
 *  maps a set of language names onto the respective encodings  
 *  @param string lang_code
 */
function get_encoding($lang_code) {
	preg_match('/(..)(.?)/', $lang_code, $hits);
	$l = strtolower($hits[1]);
	$langcodes = array (
		"bx" => 'CP1250',
		"yu" => 'CP1250',
		"pl" => 'CP1250',
		"cz" => 'CP1250',
		"sk" => 'CP1250',
		"hr" => 'CP1250',
		"sl" => 'CP1250',
		"sb" => 'CP1251',
		"by" => 'CP1251',
		"mk" => 'CP1251',
		"ru" => 'CP1251',
		"uk" => 'CP1251',
		"bg" => 'CP1251',
		"sc" => 'CP1250',
		"sx" => 'CP1250',
		"de" => 'CP1250',
		"en" => 'CP1252',
		"it" => 'CP1252',
		"fr" => 'CP1252',
		"" => 'UTF-8'
	);
	return $langcodes[$l];
}
?>