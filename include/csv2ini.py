#! /usr/bin/python
# -*- coding: utf-8 -*-
import csv, re

fieldnames=('datei', 'kuerzel', 'original', 'autor', 'titel', 'jahr', 'uebersetzer', 'verlag', 'ort', 'quelle')
reader = csv.DictReader(open('LanguagesAndResources.csv', 'r'), fieldnames)
outfile = open('LanguagesAndResources.ini', 'w')

#	if k['original'] == '1':
#		pass
#		kuerz = re.sub(r'(.*)_.*', r'\1', k['kuerzel'])
#		for l in reader:
#			m = re.match(r'(.*)_', l['kuerzel'])
#			if m:
#				if m.group(1) == kuerz:
#					l['origtitel'] = k['autor']+': '+k['titel']+'. '+k['verlag']+'. '+k['ort']+', '+k['jahr']+'.\n'
dict = {}
for k in reader:
	if k['datei'] != "DATEI":
		m = re.match(r'(.*)_(.*)', k['kuerzel'])
		if m:
			origkuerzel = m.group(1).lower()
			lang = m.group(2).lower()
			if origkuerzel in dict:
				dict[origkuerzel][lang] = {}
				for f in fieldnames:
					dict[origkuerzel][lang][f] = k[f]
				dict[origkuerzel][lang]['kurztitel'] = k['autor']+': '+re.sub(r'(.*?)(\.?)$', r'\1', k['titel'])+'. '
				if not(re.match(r'\s*$', k['verlag'], re.U)): dict[origkuerzel][lang]['kurztitel'] += k['verlag']+'. '
				if not(re.match(r'\s*$', k['ort'], re.U)): dict[origkuerzel][lang]['kurztitel'] += k['ort']+'. '
				if not(re.match(r'\s*$', k['jahr'], re.U)): dict[origkuerzel][lang]['kurztitel'] += k['jahr']+'. '
				if dict[origkuerzel][lang]['original'] == '1': dict[origkuerzel]['origtitel'] = dict[origkuerzel][lang]['kurztitel']
			else:
				dict[origkuerzel] = {}

for k in dict:
	if 'origtitel' in dict[k]:
		origtitel = dict[k]['origtitel']
	else: origtitel = ''
	for l in set(dict[k]).difference(set(['origtitel'])):
		outfile.write('['+(dict[k][l]['kuerzel']).lower()+']\n')
		for f in set(fieldnames).difference(set(['kuerzel'])):
			outfile.write(f+' = '+dict[k][l][f]+'\n')
		outfile.write('kurztitel = '+dict[k][l]['kurztitel']+'\n')
		outfile.write('origtitel = '+origtitel+'\n')
		outfile.write('\n')
