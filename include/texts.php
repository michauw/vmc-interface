<?php
include ('include/init.php');
$regTbl = new corpusTable("$REGISTRY");


if ($_POST['primlang']) {
	$primlang = $_POST['primlang'];
};

if ($_POST['languages']) {
	$langs = $_POST['languages'];
} else {
	$langs = array ();
};

$acttexts = array (); // dictionary of relevant texts
$fullselection = $_POST['fullselection'] ? $_POST['fullselection'] : "no";

if ($fullselection == "yes") {
	foreach (array_keys($regTbl->texts) as $text) {
		if ($regTbl->korptabByLang[$primlang][$text] == 1) {
			$acttexts[$text] = 1;
		};
	};
} else {
	foreach (array_keys($regTbl->texts) as $text) {
		$found = 1;
		foreach ($langs as $lang) {
			if ($regTbl->korptabByLang[$lang][$text] != 1) {
				$found = 0;
			};
		};
		if (($found == 1) && ($regTbl->korptabByLang[$primlang][$text] == 1)){
			$acttexts[$text] = 1;
		};
	};
};


echo ('<table border="1"><tr><td><a id="togglebooks">Toggle</a></td>');

foreach ($langs as $lang) {
	if ($primlang == $lang) {
		echo ('<td><span class="primlang">' .
		$lang . '</span></td>');
	} else {
		echo ('<td>' . $lang . '</td>');
	};
};
echo ('</tr>');

foreach (array_keys($acttexts) as $schluessel) {
	echo ('<tr><td title="' . $RESOURCES[strtolower($schluessel . '_' . $lang)]['origtitel'] . '"><input type="checkbox" name="selAll"');
	echo ('  value="all" checked="true" onChange="if(!checked){
					chkNone(\'selText_' . $schluessel . '[]\')} else {
					chkAll(\'selText_' . $schluessel . '[]\') }"/>' . $schluessel . '</td>');
	foreach ($langs as $lang) {
		if ($regTbl->korptabByLang[$lang][$schluessel] == 1) {
			echo ('<td title="'. $RESOURCES[strtolower($schluessel . '_' . $lang)]['kurztitel'] .'"><input type="checkbox" form="selText" name="selText_' .
			$schluessel . '[]"  value="' . $lang . '" checked="true" /></td>');
//			$schluessel . '[]"  value="' . $lang . '" checked="true" /></td>');
		} else {
			echo ('<td></td>');
		};
	};
	echo ('</tr>');
};

/**
 * query lines
 */
echo ('</table>');
?>