<?php
	//ini_set("session.use_cookies","1");
/**
 * class for constructing a corpus table and generating a query form
 * created on 16.02.2007
 * by Roland Meyer
 */
			
class corpusTable {
/**
 * @class corpusTable
 * @author Roland Meyer  
 */

/**  
 * path to the CWB registry directory
 * @var string $registry
 */ 
	var $registry;
/** 
 *  hash of hashes of the form 'text => [lang_1, ..., lang_n]', sorted by keys
 *	representing a CWB parallel corpus
 * @var array $korptab
*/
	var $korptabByText = array();

/** 
 *  hash of hashes of the form 'lang => [text_1, ..., text_n]', sorted by keys and values
 *	representing a CWB parallel corpus
 * @var array $korptabByLang
*/
	var $korptabByLang = array();

/**
 * array of arrays containing the language shortnames and longnames used in the corpus
 * @var array $languages
 */
	var $languages = array();
	
/**
 * array of arrays containing the text shortnames and longnames used in the corpus
 * @var array $texts
 */
	var $texts = array();

/**
 * constructor 
 * @method corpusTable
 * @param string registry 
 */ 
	function corpusTable ($registry){
		$this->registry = $registry;
		$this->parseRegDirectory();
	}
	
/**
 *  Parses a given CWB registry directory into $korptab  
 *  Achtung auf Varianten: letzer Kleinbuchstabe 
 */ 
	function parseRegDirectory(){
		$handle=opendir($this->registry); 
		while ($file = readdir ($handle)) {
			preg_match("/^([a-zA-Z0-9]+)\_([a-zA-Z0-9]{0,3})$/", $file, $treffer);
			if ($treffer[2]) { 
				$this->korptabByText[$treffer[1]][$treffer[2]] = 1;	
				$this->korptabByLang[$treffer[2]][$treffer[1]] = 1;
				$this->languages[$treffer[2]] = 1;
				$this->texts[$treffer[1]] = 1;			
			}
		}
		foreach (array_keys($this->languages) as $schluessel1){
			foreach (array_keys($this->korptabByText) as $schluessel2){
				if ( $this->korptabByText[$schluessel2][$schluessel1] != 1){
					 $this->korptabByText[$schluessel2][$schluessel1] = 0;
				}	
			}
		}
		foreach (array_keys($this->texts) as $schluessel1){
			foreach (array_keys($this->korptabByLang) as $schluessel2){
				if ( $this->korptabByLang[$schluessel2][$schluessel1] != 1){
					 $this->korptabByLang[$schluessel2][$schluessel1] = 0;
				}	
			}
		}	
	}	
	
/**
 * dumps out form data
 * clicking the radio button automatically selects the checkbox of the respective lang
 * clicking the checkboxes automatically adds/retracts available radiobuttons
 * @return string containing the full html form generated from the corpusTable
 */ 
	function getForm($name, $target){
// get the posted values
		if($_POST['primlang']){ $primlang = $_POST['primlang']; };
		if($_POST['langs']){ $langs = $_POST['langs']; } else { $langs = array( NULL => '');};
		$fullselection = $_POST['fullselection'] ? $_POST['fullselection'] : "no"; 
		

// add to the form data	
		$formData = '<form id="corpusform" name="tokwic" action="results.php" method="POST" target="_blank">';
//		$formData = '<form name="' . $name . '" action="' . $target . '" target="_self" method="post" >';
//		$formData .= '<table>';
//			$formData .= '<tr>';

		//$formData .= '<a id="togglelang">Toggle</a><br />';

					
/**
 *  retrieve-texts-button
 */ /*
		$formData .= '<input type="submit" name="update" value=" Update Form "/>';			
		$formData .= '</td></tr>';
		$formData .= '</table></form>';
*/				
		$formData .= '<br /><br style="clear:both;" />';
		return $formData;	
	}
}

?>